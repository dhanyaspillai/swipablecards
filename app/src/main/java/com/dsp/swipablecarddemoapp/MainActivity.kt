package com.dsp.swipablecarddemoapp

import android.os.Bundle
import android.view.View
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import com.dsp.swipablecarddemoapp.model.CardInfoResponse
import com.dsp.swipablecarddemoapp.repository.CardRepository
import com.dsp.swipablecarddemoapp.repository.CardViewModelFactory
import com.dsp.swipablecarddemoapp.viewmodel.CardViewModel
import com.yuyakaido.android.cardstackview.*


class MainActivity : AppCompatActivity(),CardStackListener {
    private val cardStackView by lazy { findViewById<CardStackView>(R.id.card_stack_view) }
    private val manager by lazy { CardStackLayoutManager(this, this) }
//    private val adapter by lazy { CardAdapter(arrayListOf()) }
    private var cardListModel: CardViewModel? = null
    private var cardRepository: CardRepository? = null
    private var list:List<CardInfoResponse>?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cardListModel =
            ViewModelProviders.of(this, CardViewModelFactory(CardRepository())).get(CardViewModel::class.java)

//                (object :ApiService{
//                override fun getData(): Call<List<CardInfoResponse>> {
//
//                    return getData()
//                }
//
//            })
          //  )


        cardListModel?.getCards()
//        cardListModel?.listOfCards?.observe(
//            this,
//            Observer(function = fun(cardList: List<CardInfoResponse>?) {
//                cardList?.let {
//                    Log.d("sos", "cardlist  $cardList")
//                    setupCardStackView()
//                    var cardAdapter: CardAdapter = CardAdapter(cardList)
//                    cardStackView.adapter = cardAdapter
//
//                }
//            })
//        )
    }

    private fun initialize() {
        manager.setStackFrom(StackFrom.None)
        manager.setVisibleCount(3)
        manager.setTranslationInterval(8.0f)
        manager.setScaleInterval(0.95f)
        manager.setSwipeThreshold(0.3f)
        manager.setMaxDegree(20.0f)
        manager.setDirections(Direction.HORIZONTAL)
        manager.setCanScrollHorizontal(true)
        manager.setCanScrollVertical(true)
        manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
        manager.setOverlayInterpolator(LinearInterpolator())
        cardStackView.layoutManager = manager
//        cardStackView.adapter = adapter
        cardStackView.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
    }
    private fun setupCardStackView() {
        initialize()
    }
    override fun onCardDisappeared(view: View?, position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCardSwiped(direction: Direction?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCardCanceled() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCardAppeared(view: View?, position: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCardRewound() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
