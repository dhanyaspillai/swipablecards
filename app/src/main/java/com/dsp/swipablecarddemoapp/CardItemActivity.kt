package com.dsp.swipablecarddemoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DiffUtil
import com.dsp.swipablecarddemoapp.model.CardData
import com.dsp.swipablecarddemoapp.model.CardInfoResponse
import com.dsp.swipablecarddemoapp.views.CardItemAdapter
import com.dsp.swipablecarddemoapp.views.SpotDiffCallback
import com.dsp.swipablecarddemoapp.webservice.ApiClient
import com.ey.weatherforecastapp.ui.main.model.web_service.ApiService
import com.facebook.drawee.backends.pipeline.Fresco
import com.yuyakaido.android.cardstackview.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.card_item_activity.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CardItemActivity : AppCompatActivity(), CardStackListener {

    private val adapter = CardItemAdapter()
    private lateinit var layoutManager: CardStackLayoutManager
    var cardList: List<CardData> = listOf()
    private val manager by lazy { CardStackLayoutManager(this, this) }
    private val cardStackView by lazy { findViewById<CardStackView>(R.id.stack_view) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fresco.initialize(this)
        setContentView(R.layout.card_item_activity)

        layoutManager = CardStackLayoutManager(this, this).apply {
            setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
            setOverlayInterpolator(LinearInterpolator())
        }

        stack_view.layoutManager = layoutManager
        stack_view.adapter = adapter
        stack_view.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
        val apiService: ApiService =
            ApiClient.getCardData().create(ApiService::class.java)
        val call: Call<CardInfoResponse> = apiService.getData()

        call.enqueue(object : Callback<CardInfoResponse> {
            override fun onFailure(call: Call<CardInfoResponse>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<CardInfoResponse>,
                response: Response<CardInfoResponse>
            ) {
                response.body()?.let {
                    var cardResponse: CardInfoResponse = response.body()!!
                    cardList = cardResponse.cardData!!
                    manager.setStackFrom(StackFrom.None)
                    manager.setVisibleCount(3)
                    manager.setTranslationInterval(8.0f)
                    manager.setScaleInterval(0.95f)
                    manager.setSwipeThreshold(0.3f)
                    manager.setMaxDegree(20.0f)
                    manager.setDirections(Direction.HORIZONTAL)
                    manager.setCanScrollHorizontal(true)
                    manager.setCanScrollVertical(true)
                    manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
                    manager.setOverlayInterpolator(LinearInterpolator())
                    cardStackView.layoutManager = manager
                    cardStackView.adapter = adapter
                    adapter.setProfiles(cardList)
                    cardStackView.itemAnimator.apply {
                        if (this is DefaultItemAnimator) {
                            supportsChangeAnimations = false
                        }
                    }
                }
            }
        })
        rewind()
    }

    override fun onCardDisappeared(view: View?, position: Int) {

    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {

    }

    override fun onCardSwiped(direction: Direction?) {
        if (manager.topPosition == adapter?.itemCount!! - 7) {
            paginate()
        }
        if (direction == Direction.VERTICAL) {
            val setting = RewindAnimationSetting.Builder()
                .setDirection(Direction.Bottom)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(DecelerateInterpolator())
                .build()
            manager.setRewindAnimationSetting(setting)
            cardStackView.rewind()
        }
    }

    override fun onCardCanceled() {

    }

    override fun onCardAppeared(view: View?, position: Int) {

    }

    override fun onCardRewound() {

    }

    private fun paginate() {
        val old = adapter?.getProfiles()
        val new = old?.plus(cardList)
        val callback = SpotDiffCallback(old!!, new!!)
        val result = DiffUtil.calculateDiff(callback)
        adapter?.setProfiles(new!!)
        result.dispatchUpdatesTo(adapter!!)
    }

    private fun rewind() {
        val rewind = findViewById<View>(R.id.rewind_button)
        rewind.setOnClickListener {
            val setting = RewindAnimationSetting.Builder()
                .setDirection(Direction.Bottom)
                .setDuration(Duration.Normal.duration)
                .setInterpolator(DecelerateInterpolator())
                .build()
            manager.setRewindAnimationSetting(setting)
            cardStackView.rewind()
        }
    }
}
