package com.dsp.swipablecarddemoapp.repository

import android.util.Log
import com.dsp.swipablecarddemoapp.model.CardInfoResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET

public class CardRepository() {
    public fun getProducts(
        onStatus:OnCardData,
        apiService: ApiService?
    ) {
        val call: Call<List<CardInfoResponse>> =
            apiService!!.getData()
        Log.d("sos", "call request: " + call.request().url())

        call.enqueue(object : Callback<List<CardInfoResponse>> {
            override fun onFailure(call: Call<List<CardInfoResponse>>, t: Throwable) {
                onStatus.onFailure()
                Log.d("sos", "call error :${t.localizedMessage}")
            }

            override fun onResponse(
                call: Call<List<CardInfoResponse>>,
                response: Response<List<CardInfoResponse>>
            ) {
                Log.d("sos", "call response :$response")
                onStatus.onSuccess((response.body() as List<CardInfoResponse>))
            }
        })
    }
    interface OnCardData {
        fun onSuccess(data: List<CardInfoResponse>)
        fun onFailure()
    }
    interface ApiService {
        @GET("fjaqJ")
        fun getData(): Call<List<CardInfoResponse>>
    }

}