package com.dsp.swipablecarddemoapp.repository

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dsp.swipablecarddemoapp.viewmodel.CardViewModel

class CardViewModelFactory(val cardRepository: CardRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CardViewModel(cardRepository) as T

    }

}