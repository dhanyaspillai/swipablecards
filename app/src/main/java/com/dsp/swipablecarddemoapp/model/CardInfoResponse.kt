package com.dsp.swipablecarddemoapp.model

import com.google.gson.annotations.SerializedName


data class CardInfoResponse(
    @SerializedName("data")
    public val cardData: List<CardData>? = null


)