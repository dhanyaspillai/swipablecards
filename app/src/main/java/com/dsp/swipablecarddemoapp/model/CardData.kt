package com.dsp.swipablecarddemoapp.model

import com.google.gson.annotations.SerializedName

data class CardData(
//        val id: Long = counter++,
//        val text: String
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("text")
    val text: String = ""
    )

