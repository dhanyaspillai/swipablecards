package com.dsp.swipablecarddemoapp.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dsp.swipablecarddemoapp.model.CardInfoResponse
import com.dsp.swipablecarddemoapp.repository.CardRepository


public open class CardViewModel(val cardRepository: CardRepository) : ViewModel() {
    var listOfCards = MutableLiveData<List<CardInfoResponse>>()

    init {
        listOfCards.value = listOf()
    }

    public fun getCards() {
        cardRepository.getProducts(object : CardRepository.OnCardData {
            override fun onSuccess(data: List<CardInfoResponse>) {
                listOfCards.value = data
            }

            override fun onFailure() {
                //REQUEST FAILED
                Log.d("sos","request failed in viewModel")
            }
        },null)
    }
}