package com.dsp.swipablecarddemoapp.webservice

import com.dsp.swipablecarddemoapp.webservice.Url.Companion.BASE_URL
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ApiClient {
    companion object {
        fun getClient(): Retrofit {
            return Retrofit.Builder().baseUrl(Url.BASE_URL)
                .client(HttpClientService.getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        fun getCardData(): Retrofit {

            val gson = GsonBuilder()

                .setLenient()
                .disableHtmlEscaping()
                .create()

            return Retrofit.Builder()

                .baseUrl(BASE_URL)
                .client(HttpClientService.getUnsafeOkHttpClient())
                .addConverterFactory(CustomConverterFactory())
                .build()
        }
    }
}