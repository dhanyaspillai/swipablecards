package com.ey.weatherforecastapp.ui.main.model.web_service

import com.dsp.swipablecarddemoapp.model.CardInfoResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("fjaqJ")
    fun getData(): Call<CardInfoResponse>
}