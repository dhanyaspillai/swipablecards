package com.dsp.swipablecarddemoapp.views

import androidx.recyclerview.widget.DiffUtil
import com.dsp.swipablecarddemoapp.model.CardData
import com.dsp.swipablecarddemoapp.model.CardInfoResponse

class SpotDiffCallback(
        private val old: List<CardData>,
        private val new: List<CardData>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return old.size
    }

    override fun getNewListSize(): Int {
        return new.size
    }

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition].id == new[newPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition] == new[newPosition]
    }

}
