package com.dsp.swipablecarddemoapp.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dsp.swipablecarddemoapp.R
import com.dsp.swipablecarddemoapp.databinding.RecyclerViewProfileBinding
import com.dsp.swipablecarddemoapp.model.CardData

class CardItemAdapter : RecyclerView.Adapter<CardItemAdapter.ProfileViewHolder>() {

    private var profiles: List<CardData>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProfileViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.recycler_view_profile,
            parent,
            false
        )
    )

    override fun getItemCount() = profiles?.size ?: 0

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        profiles?.let {
            holder.binding.profile = it[position]
            holder.binding.executePendingBindings()
        }
    }

    fun setProfiles(profiles: List<CardData>) {
        this.profiles = profiles
        notifyDataSetChanged()
    }


    fun getProfiles(): List<CardData> {
        return profiles!!
    }
    inner class ProfileViewHolder(val binding: RecyclerViewProfileBinding) :
        RecyclerView.ViewHolder(binding.root)

}